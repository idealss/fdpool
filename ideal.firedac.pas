unit ideal.firedac;

interface

uses
  System.Sysutils,
  System.Classes,
  cocinasync.collections,
  FireDAC.Comp.Client;

type
  TWithQueryHandler = reference to procedure(Qry : TFDQuery);
  TWithQueryResult<T> = reference to function(Qry : TFDQuery) : T;
  TWithStoredProcHandler = reference to procedure(Script : TFDStoredProc);
  TConnectionHandler = reference to procedure(Connection : TFDConnection);

  TContext = class(TInterfacedObject)
  private
    FConnection : TFDConnection;
    FQueries : TQueue<TFDQuery>;
    FStoredProcs : TQueue<TFDStoredProc>;
  public
    function AcquireQuery : TFDQuery;
    procedure ReleaseQuery(Qry : TFDQuery);
    procedure WithQuery(Handler : TWithQueryHandler);

    function AcquireStoredProc : TFDStoredProc;
    procedure ReleaseStoredProc(Proc : TFDStoredProc);
    procedure WithStoredProc(Handler : TWithStoredProcHandler);

    function InTransaction : boolean;
    procedure StartTransaction;
    procedure Commit;
    procedure Rollback;

    constructor Create(const DriverID : string; Options : TStrings; ConnectionHandler : TConnectionHandler = nil; const silent: boolean = true); reintroduce;
    destructor Destroy; override;
  end;

  TUseContextHandler = reference to procedure(const Context : TContext);

  TPool = class(TInterfacedObject)
  private
    FDriver : string;
    FContexts : TQueue<TContext>;
    FOptions: TStringList;
    FConnectionHandler : TConnectionHandler;
    FSilent: Boolean;
  public
    procedure Initialize(Options : TStrings);

    function Acquire : TContext;
    procedure Release(Context : TContext);
    procedure Use(Handler : TUseContextHandler);
    procedure Transaction(Handler : TUseContextHandler);

    procedure Open(const SQL : string; Handler : TWithQueryHandler);
    procedure ExecSQL(const SQL : string);

    function LookupValue<T>(handler : TWithQueryResult<T>) : T; overload;
    procedure WithQuery(handler : TWithQueryHandler);

    constructor Create(Driver : string; ConnectionHandler : TConnectionHandler; silent: boolean = true); reintroduce;
    destructor Destroy; override;
  end;


function NewPool(Driver : String; Options : TStrings = nil; ConnectionHandler : TConnectionHandler = nil; silent: boolean = true) : TPool;

const
  MAX_QUERY_POOL_SIZE = 4096;
  MAX_PROC_POOL_SIZE = 2048;
  MAX_CONNECTION_POOL_SIZE = 2048;
  ROOM_TO_SPARE_IN_POOL = 100;

implementation

uses
  FireDAC.Stan.Def,
  FireDAC.DApt,
  FireDAC.Stan.Async,
  Data.DB,
  FireDAC.Comp.DataSet,
  ideal.logger,
  System.DateUtils,
  FireDAC.Stan.Param;

type
  TIdealFDQuery = class(TFDQuery)
  private
  public
    procedure Execute(ATimes: Integer = 0; AOffset: Integer = 0); override;
    procedure OpenCursor(InfoQuery: Boolean = False); override;
  end;

  TIdealFDStoredProc = class(TFDStoredProc)
  private
  public
    procedure Execute(ATimes: Integer = 0; AOffset: Integer = 0); override;
    procedure OpenCursor(InfoQuery: Boolean = False); override;
  end;

function NewPool(Driver : String; Options : TStrings = nil; ConnectionHandler : TConnectionHandler = nil; silent: boolean = true) : TPool;
begin
  Result := TPool.Create(Driver, ConnectionHandler, silent);
  if Options <> nil then
    Result.Initialize(Options);
end;

{ TPool }

function TPool.Acquire: TContext;
begin
  Result := FContexts.Dequeue;
  if Result = nil then
  begin
    Result := TContext.Create(FDriver, FOptions, FConnectionHandler, FSilent);
    try
      Result.FConnection.Open;
    except
      Result.Free;
      Result := nil;
      Raise;
    end;
  end;
end;

constructor TPool.Create(Driver : string; ConnectionHandler : TConnectionHandler; silent: boolean = true);
begin
  inherited Create;
  FDriver := Driver;
  FContexts := TQueue<TContext>.Create(MAX_CONNECTION_POOL_SIZE);
  FOptions := TStringList.Create;
  FConnectionHandler := ConnectionHandler;
  FSilent := silent;
end;

destructor TPool.Destroy;
begin
  while FContexts.Count > 0 do
    FContexts.Dequeue.Free;
  FContexts.Free;
  FOptions.Free;
  inherited;
end;

procedure TPool.ExecSQL(const SQL: string);
var
  cxt : TContext;
  qry: TFDQuery;
begin
  cxt := Acquire;
  try
    qry := cxt.AcquireQuery;
    try
      qry.SQL.Text := SQL;
      qry.ExecSQL;
    finally
      cxt.ReleaseQuery(qry);
    end;
  finally
    Release(cxt);
  end;
end;

procedure TPool.Initialize(Options: TStrings);
begin
  FOptions.Assign(Options);
end;

procedure TPool.Open(const SQL: string; Handler: TWithQueryHandler);
var
  cxt : TContext;
  qry: TFDQuery;
begin
  cxt := Acquire;
  try
    qry := cxt.AcquireQuery;
    try
      qry.SQL.Text := SQL;
      qry.Open;
      Handler(qry);
    finally
      cxt.ReleaseQuery(qry);
    end;
  finally
    Release(cxt);
  end;
end;

procedure TPool.Release(Context: TContext);
begin
  if FContexts.Count < MAX_CONNECTION_POOL_SIZE - ROOM_TO_SPARE_IN_POOL then
    FContexts.Enqueue(Context)
  else
    FContexts.Free;
end;

function TPool.LookupValue<T>( handler : TWithQueryResult<T>): T;
var
  cxt : TContext;
  qry: TFDQuery;
begin
  cxt := Acquire;
  try
    qry := cxt.AcquireQuery;
    try
      Result := handler(qry);
    finally
      cxt.ReleaseQuery(qry);
    end;
  finally
    Release(cxt);
  end;
end;

procedure TPool.Transaction(Handler: TUseContextHandler);
var
  cxt : TContext;
  bCommitOrRollback : boolean;
begin
  bCommitOrRollback := false;
  cxt := Acquire;
  try
    if not cxt.InTransaction then
    begin
      cxt.StartTransaction;
      bCommitOrRollback := True;
    end;
    try
      Handler(cxt);
      if bCommitOrRollback then
        cxt.Commit;
    except
      on e: Exception do
      begin
        try
          if bCommitOrRollback then
            cxt.Rollback;
        finally
        end;
        raise;
      end;
    end;
  finally
    Release(cxt);
  end;
end;

procedure TPool.Use(Handler: TUseContextHandler);
var
  cxt : TContext;
begin
  cxt := Acquire;
  try
    Handler(cxt);
  finally
    Release(cxt);
  end;
end;

procedure TPool.WithQuery(handler: TWithQueryHandler);
var
  cxt : TContext;
  qry: TFDQuery;
begin
  cxt := Acquire;
  try
    qry := cxt.AcquireQuery;
    try
      handler(qry);
    finally
      cxt.ReleaseQuery(qry);
    end;
  finally
    Release(cxt);
  end;
end;

{ TContext }

function TContext.AcquireQuery: TFDQuery;
begin
  Result := FQueries.Dequeue;
  if Result = nil then
  begin
    Result := TIdealFDQuery.Create(nil);
    Result.Connection := FConnection;
  end;
end;

function TContext.AcquireStoredProc: TFDStoredProc;
begin
  Result := FStoredProcs.Dequeue;
  if Result = nil then
  begin
    Result := TIdealFDStoredProc.Create(nil);
    Result.Connection := FConnection;
  end;
end;

constructor TContext.Create(const DriverID : string; Options : TStrings; ConnectionHandler : TConnectionHandler = nil; const silent: boolean = true);
begin
  inherited Create;
  FQueries := TQueue<TFDQuery>.Create(MAX_QUERY_POOL_SIZE);
  FStoredProcs := TQueue<TFDStoredProc>.Create(MAX_PROC_POOL_SIZE);
  FConnection := TFDConnection.Create(nil);
  FConnection.Transaction := TFDTransaction.Create(FConnection);
  FConnection.Params.AddStrings(Options);
  FConnection.Params.DriverID := DriverID;
  FConnection.LoginPrompt := False;
  FConnection.ResourceOptions.SilentMode := silent;
  if Assigned(ConnectionHandler) then
    ConnectionHandler(FConnection);
  //FConnection.Open;
end;

destructor TContext.Destroy;
begin
  while FQueries.Count > 0 do
    FQueries.Dequeue.Free;
  FQueries.Free;
  while FStoredProcs.Count > 0 do
    FStoredProcs.Dequeue.Free;
  FStoredProcs.Free;
  FConnection.Free;
  inherited;
end;

function TContext.InTransaction: boolean;
begin
  Result := FConnection.Transaction.Active;
end;

procedure TContext.ReleaseQuery(Qry: TFDQuery);
begin
  if FQueries.Count < MAX_QUERY_POOL_SIZE - ROOM_TO_SPARE_IN_POOL then
  begin
    if Qry.Active then
      Qry.Close;
    if Qry.Prepared then
      Qry.Unprepare;
    Qry.Filter := '';
    Qry.IndexName := '';
    Qry.IndexFieldNames := '';
    Qry.SQL.Clear;
    FQueries.Enqueue(Qry);
  end else
    Qry.Free;
end;

procedure TContext.ReleaseStoredProc(Proc: TFDStoredProc);
begin
  if FStoredProcs.Count < MAX_PROC_POOL_SIZE - ROOM_TO_SPARE_IN_POOL then
  begin
    if Proc.Active then
      Proc.Close;
    FStoredProcs.Enqueue(Proc);
  end else
    Proc.Free;
end;

procedure TContext.StartTransaction;
begin
  FConnection.Transaction.StartTransaction;
end;

procedure TContext.Commit;
begin
  FConnection.Transaction.Commit;
end;

procedure TContext.Rollback;
begin
  FConnection.Transaction.Rollback;
end;

procedure TContext.WithQuery(Handler: TWithQueryHandler);
var
  qry : TFDQuery;
begin
  qry := AcquireQuery;
  try
    Handler(qry);
  finally
    ReleaseQuery(qry);
  end;
end;

procedure TContext.WithStoredProc(Handler: TWithStoredProcHandler);
var
  sp : TFDStoredProc;
begin
  sp := AcquireStoredProc;
  try
    Handler(sp);
  finally
    ReleaseStoredProc(sp);
  end;
end;

{ TIdealFDQuery }

procedure TIdealFDQuery.Execute(ATimes, AOffset: Integer);
var
  paramstr: string;
  I: Integer;
begin
  paramstr := '';
  for I := 0 to Params.Count - 1 do
    paramstr := paramstr + #10#13 + Params[I].Name + '=' + Params[I].AsString;
  TLogger.Profile('Query: '+#10#13+SQL.Text+paramstr);
  inherited;
end;

procedure TIdealFDQuery.OpenCursor(InfoQuery: Boolean);
var
  paramstr: string;
  I: Integer;
begin
  paramstr := '';
  for I := 0 to Params.Count - 1 do
    paramstr := paramstr + #10#13 + Params[I].Name + '=' + Params[I].AsString;
  TLogger.Profile('Query: '+#10#13+SQL.Text+paramstr);
  inherited;
end;

{ TIdealFDStoredProc }

procedure TIdealFDStoredProc.Execute(ATimes, AOffset: Integer);
var
  paramstr: string;
  I: Integer;
begin
  paramstr := '';
  for I := 0 to Params.Count - 1 do
    paramstr := paramstr + #10#13 + Params[I].Name + '=' + Params[I].AsString;
  TLogger.Profile('Proc: '+self.StoredProcName+paramstr);
  inherited;
end;

procedure TIdealFDStoredProc.OpenCursor(InfoQuery: Boolean);
var
  paramstr: string;
  I: Integer;
begin
  paramstr := '';
  for I := 0 to Params.Count - 1 do
    paramstr := paramstr + #10#13 + Params[I].Name + '=' + Params[I].AsString;
  TLogger.Profile('Proc: '+self.StoredProcName+paramstr);
  inherited;
end;

end.
